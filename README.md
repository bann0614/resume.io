# 个人简历模板

## 简历模版介绍
本人简历基于[Funday](https://gitee.com/xiaodan_yu/resume.io)修改而来，本模版基于[jekyll](https://jekyllcn.com/)。
添加了[Layui](https://layuion.com/)实现部分效果，[clipboard](https://clipboardjs.com/)插件实现复制功能

Jekyll（发音/'dʒiːk əl/，"杰克尔"）是一个静态站点生成器，它会根据网页源码生成静态文件。它提供了模板、变量、插件等功能，所以实际上可以用来编写整个网站。

